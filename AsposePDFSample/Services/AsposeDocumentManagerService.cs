﻿using AsposePDFSample.PDF;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AsposePDFSample.Services
{
    public class AsposeDocumentManagerService
    {

        public MemoryStream AddFooterText(string originalExtension, MemoryStream sourceStream, string footerText)
        {
            MemoryStream outputStream = new MemoryStream();
            switch (originalExtension.ToLower())
            {
                case ".pdf":
                    PdfDocumentManager pdfDocumentManager = new PdfDocumentManager();
                    pdfDocumentManager.AddFooterText(outputStream, sourceStream, footerText);
                    break;

                default:
                    throw new Exception(originalExtension);

            }
            return outputStream;
        }
    }
}