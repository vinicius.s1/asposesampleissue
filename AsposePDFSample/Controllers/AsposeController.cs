﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Aspose.Pdf;
using Aspose.Pdf.Text;
using AsposePDFSample.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AsposePDFSample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AsposeController : ControllerBase
    {

        [HttpGet]
        public IActionResult GenerateFooterPDF()
        {
            string footerText = "Insert in footer";
            AsposeDocumentManagerService asposeDocumentManagerService = new AsposeDocumentManagerService();

            using (MemoryStream inputStream = new MemoryStream(Properties.Resources.PdfParaAdicionarFooter1Pagina))
            {
                using (MemoryStream outputStream = asposeDocumentManagerService.AddFooterText(".pdf", inputStream, footerText))
                {

                    FileStream file = new FileStream("c:\\file.pdf", FileMode.Create, FileAccess.Write);
                    outputStream.WriteTo(file);
                    file.Close();
                }
            }
            return Ok("Generated PDF >> c:\\file.pdf");
        }

        private Boolean CheckPageHasFooterText(Page page, string footerText)
        {
            TextAbsorber absorber = new TextAbsorber();
            absorber.TextSearchOptions.LimitToPageBounds = true;
            absorber.TextSearchOptions.Rectangle = new Aspose.Pdf.Rectangle(0, 0, page.PageInfo.Width, 25);
            page.Accept(absorber);

            if (!absorber.Text.Contains(footerText)) return true;
            return false;

        }
    }
}
