﻿using Aspose.Pdf;
using Aspose.Pdf.Facades;
using Aspose.Pdf.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsposePDFSample.PDF
{
    public class PdfDocumentManager
    {
        internal void AddFooterText(Stream outputStream, Stream inputStream, string footerText)
        {

            StringBuilder text = new StringBuilder();

            foreach (string lineText in footerText.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
                text.Append(lineText);

            // Open document
            Document pdfDocument = new Document(inputStream);

            // Create footer
            TextStamp textStamp = new TextStamp(text.ToString());
            // Set properties of the stamp

            Aspose.Pdf.Text.Font font = null;
            Aspose.Pdf.Text.FontRepository.LoadFonts();
            font = Aspose.Pdf.Text.FontRepository.OpenFont("/usr/share/fonts/truetype/msttcorefonts/comicbd.ttf");
            textStamp.TextState.Font = font;
            textStamp.BottomMargin = 10;
            textStamp.TextState.FontSize = 7F;
            textStamp.TextState.BackgroundColor = Color.Transparent;
            textStamp.TextState.ForegroundColor = Color.Gray;
            textStamp.WordWrap = true;
            textStamp.HorizontalAlignment = HorizontalAlignment.Center;
            textStamp.VerticalAlignment = VerticalAlignment.Bottom;
            // Add footer on all pages
            foreach (Page page in pdfDocument.Pages)
            {
                page.AddStamp(textStamp);
            }

            TextAbsorber absorber = new TextAbsorber();
            absorber.TextSearchOptions.LimitToPageBounds = true;
            absorber.TextSearchOptions.Rectangle = new Aspose.Pdf.Rectangle(0, 0, pdfDocument.Pages.FirstOrDefault().PageInfo.Width, 25);
            pdfDocument.Pages.FirstOrDefault().Accept(absorber);

            Console.WriteLine("Font name: " + font.FontName);
            Console.WriteLine("PDF footer text: " + absorber.Text);
            Console.WriteLine("Stream can read: " + outputStream.CanRead);
            pdfDocument.Save(outputStream);
        }
    }
}

    
