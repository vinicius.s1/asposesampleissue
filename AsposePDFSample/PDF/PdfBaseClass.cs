﻿using Aspose.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AsposePDFSample.PDF
{
    public class PdfBaseClass
    {
        internal PdfBaseClass()
        {
            using (MemoryStream licenseMemoryStream = new MemoryStream(Properties.Resources.Aspose_Total))
            {
                License license = new License();
                license.Embedded = true;
                license.SetLicense(licenseMemoryStream);
            }
        }
    }
}
